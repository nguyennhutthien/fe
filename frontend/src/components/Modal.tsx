import * as React from 'react'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import Slide from '@mui/material/Slide'
import { TransitionProps } from '@mui/material/transitions'
import { InputLabel, Stack, TextField } from '@mui/material'
import { Action } from './Table'
import moment from 'moment'

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction='up' ref={ref} {...props} />
})

interface IProps {
  isOpen: boolean
  title: string
  action: string
  onClose: Function
  onSubmit: Function
  onCancel: Function
  warning?: string
  data?: Array<any>
  item?: any
}

export default function Modal(props: IProps) {
  const { isOpen, title, onSubmit, onClose, item, action, warning } = props

  const [newItem, setNewItem] = React.useState<any>()

  React.useEffect(() => {
    setNewItem({
      shop_name: item?.shop_name,
      phone_number: item?.phone_number,
      address: item?.address,
      start_date: item?.start_date,
      end_date: item?.end_date,
      status: item?.status ?? '',
      review_amount: item?.review_amount,
      note: item?.note,
      _id: item?._id,
    })
  }, [item])

  return (
    <div>
      <Dialog
        open={isOpen}
        TransitionComponent={Transition}
        keepMounted
        onClose={() => {
          onClose({ isOpen: false })
          setNewItem({
            isOpen: false,
            title: '',
            action: '',
            warning: '',
            data: [],
            item: null,
          })
        }}
        aria-describedby='alert-dialog-slide-description'
        fullWidth
      >
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          {action === Action.Create || action === Action.Update ? (
            <Stack m={1} spacing={1}>
              <InputLabel htmlFor='shop_name'>Tên cửa hàng</InputLabel>
              <TextField
                id='shop_name'
                variant='outlined'
                value={newItem?.shop_name}
                onChange={(e) =>
                  setNewItem({ ...newItem, shop_name: e.target.value })
                }
              />
              <InputLabel htmlFor='phone_number'>Số điện thoại</InputLabel>
              <TextField
                id='phone_number'
                variant='outlined'
                value={newItem?.phone_number}
                onChange={(e) =>
                  setNewItem({ ...newItem, phone_number: e.target.value })
                }
              />
              <InputLabel htmlFor='address'>Địa chỉ</InputLabel>
              <TextField
                id='address'
                variant='outlined'
                value={newItem?.address}
                onChange={(e) =>
                  setNewItem({ ...newItem, address: e.target.value })
                }
              />
              <InputLabel htmlFor='review_amount'>Số lần</InputLabel>
              <TextField
                id='review_amount'
                type='number'
                variant='outlined'
                value={newItem?.review_amount}
                disabled={action === Action.Update ? true : false}
                onChange={(e) =>
                  setNewItem({ ...newItem, review_amount: e.target.value })
                }
              />
              <InputLabel htmlFor='note'>Ghi chú</InputLabel>
              <TextField
                id='note'
                variant='outlined'
                value={newItem?.note}
                onChange={(e) =>
                  setNewItem({ ...newItem, note: e.target.value })
                }
              />
              <InputLabel htmlFor='start_date'>Ngày bắt đầu</InputLabel>
              <TextField
                id='start_date'
                type='date'
                variant='outlined'
                disabled={action === Action.Update ? true : false}
                onChange={(e) =>
                  setNewItem({ ...newItem, start_date: e.target.value })
                }
              />
              <InputLabel htmlFor='end_date'>Ngày kết thúc</InputLabel>
              <TextField
                id='end_date'
                type='date'
                disabled={action === Action.Update ? true : false}
                variant='outlined'
                onChange={(e) =>
                  setNewItem({ ...newItem, end_date: e.target.value })
                }
              />
            </Stack>
          ) : (
            warning
          )}
        </DialogContent>
        <DialogActions>
          <Button
            variant='contained'
            color='error'
            onClick={() => {
              onClose({ isOpen: false })
              setNewItem(null)
            }}
          >
            Huỷ
          </Button>
          <Button
            variant='contained'
            color='primary'
            onClick={() => {
              switch (action) {
                case Action.Create:
                  onSubmit(newItem)
                  break
                case Action.Update:
                  onSubmit(newItem)
                  break
                case Action.Delete:
                  console.log(item)

                  onSubmit(item)
                  break
                case Action.Export:
                  onSubmit(item)
                  break
                default:
                  break
              }

              setNewItem(null)
            }}
          >
            OK
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
