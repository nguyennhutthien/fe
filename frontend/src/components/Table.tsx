import * as React from 'react'
import Box from '@mui/material/Box'
import Collapse from '@mui/material/Collapse'
import IconButton from '@mui/material/IconButton'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Typography from '@mui/material/Typography'
import Paper from '@mui/material/Paper'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'
import AddIcon from '@mui/icons-material/Add'
import { Chip, Stack } from '@mui/material'
import ClearIcon from '@mui/icons-material/Clear'
import EditIcon from '@mui/icons-material/Edit'
import GetAppIcon from '@mui/icons-material/GetApp'
import moment from 'moment'
import Modal from './Modal'
import {
  createReview,
  deleteReview,
  updateReview,
} from '../services/reviews.service'
import TablePagination from '@mui/material/TablePagination'

export enum Action {
  Create = 'CRE',
  Delete = 'DEL',
  Update = 'UPD',
  Export = 'EXP',
}

function Row(props: { row: ReturnType<any>; handle: Function }) {
  const { row, handle } = props
  const [open, setOpen] = React.useState(false)

  return (
    <React.Fragment>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell width='10%'>
          <IconButton
            aria-label='expand row'
            size='small'
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component='th' scope='row' width='15%'>
          {row.shop_name}
        </TableCell>
        <TableCell component='th' scope='row' width='10%'>
          {row.phone_number}
        </TableCell>
        <TableCell component='th' scope='row' width='25%'>
          {row.address}
        </TableCell>
        <TableCell component='th' scope='row' width='10%'>
          {row.start_date}
        </TableCell>
        <TableCell width='10%'>{row.end_date}</TableCell>
        <TableCell width='10%' align='center'>
          <Chip
            label={row?.status ? 'Hiệu lực' : 'Kết thúc'}
            color={row?.status ? 'success' : 'error'}
          />
        </TableCell>
        <TableCell width='5%' align='center'>
          {row.review_amount}
        </TableCell>
        <TableCell width='5%' align='center'>
          {row?.note ? (
            <a href={`${row.note}`} target='_blank'>
              Link
            </a>
          ) : (
            ''
          )}
        </TableCell>
        <TableCell width='10%'>
          <Stack direction='row' alignItems='center'>
            <IconButton
              onClick={() =>
                handle({
                  isOpen: true,
                  title: 'Xoá cửa hàng',
                  warning: `Bạn có chắc muốn xoá cửa hàng ${row.shop_name}?`,
                  action: Action.Delete,
                  item: row._id,
                })
              }
              aria-label='delete'
              size='medium'
            >
              <ClearIcon fontSize='inherit' />
            </IconButton>
            <IconButton
              onClick={() =>
                handle({
                  isOpen: true,
                  title: 'Chỉnh sửa cửa hàng',
                  action: Action.Update,
                  item: row,
                })
              }
              aria-label='delete'
              size='medium'
            >
              <EditIcon fontSize='inherit' />
            </IconButton>
          </Stack>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout='auto' unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Typography variant='h6' gutterBottom component='div'>
                Lịch chạy
              </Typography>
              <Table size='small' aria-label='purchases'>
                {/* <TableHead>
                  <TableRow>
                    <TableCell>Date</TableCell>
                    <TableCell>Customer</TableCell>
                    <TableCell align='right'>Amount</TableCell>
                  </TableRow>
                </TableHead> */}
                <TableBody>
                  {/* {row.history.map((historyRow) => (
                    <TableRow key={historyRow.date}>
                      <TableCell component='th' scope='row'>
                        {historyRow.date}
                      </TableCell>
                      <TableCell>{historyRow.customerId}</TableCell>
                      <TableCell align='right'>{historyRow.amount}</TableCell>
                      <TableCell align='right'>
                        {Math.round(historyRow.amount * row.price * 100) / 100}
                      </TableCell>
                    </TableRow>
                  ))} */}
                  Coming soon...
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  )
}

export default function CollapsibleTable(props: any) {
  const { data, fetch, pagin, setPagin, setNoti } = props
  const [openModal, setOpenModal] = React.useState<any>({
    isOpen: false,
    title: '',
    action: '',
    warning: '',
    data: [],
    item: null,
  })

  return (
    <Box width='90%'>
      <TableContainer sx={{ margin: 10 }} component={Paper}>
        <Modal
          {...openModal}
          onClose={(data: any) => setOpenModal({ ...data })}
          onSubmit={async (data: any) => {
            switch (openModal.action) {
              case Action.Create:
                try {
                  const rs = await createReview(data)
                  if (rs.is_success) {
                    setOpenModal({
                      isOpen: false,
                      title: '',
                      action: '',
                      warning: '',
                      data: [],
                      item: null,
                    })
                    setNoti({
                      open: true,
                      msg: 'Tạo cửa hàng thành công!',
                      severity: 'success',
                    })
                    fetch()
                  } else {
                    setOpenModal({
                      isOpen: false,
                      title: '',
                      action: '',
                      warning: '',
                      data: [],
                      item: null,
                    })
                    setNoti({
                      open: true,
                      msg: rs.error?.error_message,
                      severity: 'error',
                    })
                  }
                } catch (error: any) {
                  setOpenModal({
                    isOpen: false,
                    title: '',
                    action: '',
                    warning: '',
                    data: [],
                    item: null,
                  })

                  setNoti({
                    open: true,
                    msg: error?.message,
                    severity: 'error',
                  })
                }

                break
              case Action.Update:
                try {
                  const rs = await updateReview(data._id, data)
                  if (rs.is_success) {
                    setOpenModal({
                      isOpen: false,
                      title: '',
                      action: '',
                      warning: '',
                      data: [],
                      item: null,
                    })
                    setNoti({
                      open: true,
                      msg: 'Cập nhật cửa hàng thành công!',
                      severity: 'success',
                    })
                    fetch()
                  } else {
                    setOpenModal({
                      isOpen: false,
                      title: '',
                      action: '',
                      warning: '',
                      data: [],
                      item: null,
                    })
                    setNoti({
                      open: true,
                      msg: rs.error?.error_message,
                      severity: 'error',
                    })
                  }
                } catch (error: any) {
                  setOpenModal({
                    isOpen: false,
                    title: '',
                    action: '',
                    warning: '',
                    data: [],
                    item: null,
                  })

                  setNoti({
                    open: true,
                    msg: error?.message,
                    severity: 'error',
                  })
                }
                break
              case Action.Delete:
                try {
                  const rs = await deleteReview(data)
                  if (rs.is_success) {
                    setOpenModal({
                      isOpen: false,
                      title: '',
                      action: '',
                      warning: '',
                      data: [],
                      item: null,
                    })

                    setNoti({
                      open: true,
                      msg: 'Xoá cửa hàng thành công!',
                      severity: 'success',
                    })
                    fetch()
                  } else {
                    setOpenModal({
                      isOpen: false,
                      title: '',
                      action: '',
                      warning: '',
                      data: [],
                      item: null,
                    })
                    setNoti({
                      open: true,
                      msg: rs.error?.error_message,
                      severity: 'error',
                    })
                  }
                } catch (error: any) {
                  setOpenModal({
                    isOpen: false,
                    title: '',
                    action: '',
                    warning: '',
                    data: [],
                    item: null,
                  })

                  setNoti({
                    open: true,
                    msg: error?.message,
                    severity: 'error',
                  })
                }
                break

              default:
                break
            }
          }}
        />
        <Table aria-label='collapsible table'>
          <TableHead>
            <TableRow>
              <TableCell>
                <IconButton
                  href={`${process.env.REACT_APP_DOMAIN}/api/reviewer/export-file`}
                  aria-label='add'
                  size='small'
                >
                  <GetAppIcon fontSize='large' />
                </IconButton>
              </TableCell>
              <TableCell>Cửa hàng</TableCell>
              <TableCell>Số điện thoại</TableCell>
              <TableCell>Địa chỉ</TableCell>
              <TableCell>Bắt đầu</TableCell>
              <TableCell>Kết thúc</TableCell>
              <TableCell align='center'>Trạng thái</TableCell>
              <TableCell align='center'>Số lần</TableCell>
              <TableCell align='center'>Ghi chú</TableCell>
              <TableCell align='center'>
                <IconButton
                  onClick={() =>
                    setOpenModal({
                      isOpen: true,
                      title: 'Tạo cửa hàng',
                      action: Action.Create,
                    })
                  }
                  aria-label='add'
                  size='small'
                >
                  <AddIcon fontSize='large' />
                </IconButton>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody sx={{ color: 'white' }}>
            {(data.reviewers || []).map((row: any) => (
              <Row
                key={row.name}
                row={row}
                handle={(data: any) => setOpenModal(data)}
              />
            ))}
          </TableBody>
        </Table>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component='div'
          count={data.total}
          rowsPerPage={pagin.limit}
          page={pagin.page}
          onPageChange={(e: unknown, newPage: number) =>
            setPagin({ type: 'page', page: newPage })
          }
          onRowsPerPageChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setPagin({
              type: 'limit',
              page: 0,
              limit: parseInt(e.target.value, 10),
            })
          }}
        />
      </TableContainer>
    </Box>
  )
}
