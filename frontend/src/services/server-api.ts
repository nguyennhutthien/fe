import { Fetch } from '../util/fetch'

export const serverAPI: Fetch = new Fetch(
  `${process.env.REACT_APP_DOMAIN}/api/`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  }
)
