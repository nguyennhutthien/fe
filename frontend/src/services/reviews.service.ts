import { serverAPI } from './server-api'
import moment from 'moment'

export const getReviews = async (param: {
  page: number
  limit: number
}): Promise<{
  is_success: boolean
  data: any | null
  error: any | null
}> => serverAPI.get(`reviewer?page=${param.page}&limit=${param.limit}`)

export const createReview = async (
  data: any
): Promise<{
  is_success: boolean
  data: any | null
  error: any | null
}> => {
  const { start_date, end_date, review_amount } = data

  const newData = {
    ...data,
    start_date: new Date(start_date).toISOString(),
    end_date: new Date(end_date).toISOString(),
    review_amount: Number(review_amount),
  }
  return serverAPI.post('reviewer', newData)
}

export const updateReview = async (
  id: string,
  data: any
): Promise<{
  is_success: boolean
  data: any | null
  error: any | null
}> => {
  const keys = ['start_date', 'end_date', 'review_amount', '_id', 'status']
  const newData: any = {}
  Object.keys(data).forEach((key) => {
    if (!keys.includes(key)) {
      newData[key] = data[key]
    }
  })

  return serverAPI.put(`reviewer/${id}`, newData)
}

export const deleteReview = async (
  id: string
): Promise<{
  is_success: boolean
  data: any | null
  error: any | null
}> => {
  return serverAPI.delete(`reviewer/${id}`)
}
