import { Alert, Grid, Snackbar } from '@mui/material'
import React, { useEffect, useState } from 'react'
import BasicTable from './components/Table'
import { getReviews } from './services/reviews.service'
import Slide from '@mui/material/Slide'

function TransitionLeft(props: any) {
  return <Slide {...props} direction='right' />
}

function App() {
  const [data, setData] = useState([])
  const [isFetch, setIsFetch] = React.useState(false)
  const [pagin, setPagin] = React.useState<any>({
    page: 0,
    limit: 5,
  })
  const [noti, setNoti] = React.useState<{
    open: boolean
    msg: string
    severity: 'success' | 'error' | undefined
  }>({
    open: false,
    msg: '',
    severity: undefined,
  })
  useEffect(() => {
    getReviews(pagin).then((data) => setData(data.data))
  }, [isFetch, pagin])

  return (
    <Grid container bgcolor='#282c34' minHeight='100vh'>
      <Snackbar
        TransitionComponent={TransitionLeft}
        open={noti.open}
        autoHideDuration={2000}
        onClose={() => setNoti({ open: false, msg: '', severity: undefined })}
      >
        <Alert
          onClose={() => setNoti({ open: false, msg: '', severity: undefined })}
          severity={noti.severity}
          sx={{ width: '100%' }}
        >
          {noti.msg}
        </Alert>
      </Snackbar>
      <BasicTable
        data={data}
        fetch={() => setIsFetch(!isFetch)}
        pagin={pagin}
        setPagin={(data: any) => {
          if (data.type === 'page') {
            setPagin({
              ...pagin,
              page: +data.page,
            })
          } else if (data.type === 'limit') {
            setPagin({
              ...pagin,
              ...data,
            })
          }
        }}
        setNoti={(data: any) => setNoti(data)}
      />
    </Grid>
  )
}
export default App
